<?php

use Illuminate\Http\Request;


Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('user/login','API\UserController@login');
Route::post('user/register','API\UserController@register');
Route::get('user/role/{role}','API\UserController@role');
// Route::get('user/show/{id}','API\UserController@show');
Route::get('user/getrole/{email}','API\UserController@getRole');
Route::resource('user','API\UserController');
Route::get('secretkeys', 'API\SecretKeyController@getSecretKeys');
Route::resource('alat','AlatController');
Route::post('/tambah/{id_kategori}','AlatController@tambah');
Route::resource('bidang','BidangController');
Route::resource('maintenance','MaintenanceController');
Route::resource('instalasi','InstalasiController');
Route::resource('ruang','RuangController');
// Route::get('/showbykat/','AlatController@showbykat');
Route::get('showbykat/{id_kategori}', AlatController::class . '@showbykat');
// Route::post('alat/edit/{id_alat}', 'AlatController@edit');
Route::resource('kategorialat','KategoriAlatController');
// Route::resource('alatmasuk','AlatMasukController');
Route::resource('alatkeluar','AlatKeluarController');
Route::post('/keluar/{id}','AlatKeluarController@keluar');
// Route::resource('user','UserMobileController');
Route::resource('kalibrasi','KalibrasiController');
Route::resource('distributor','DistributorController');
Route::get('/combo/bidang', 'ComboController@bidang');
Route::get('/combo/instalasi', 'ComboController@instalasi');
Route::any('/getdata', 'AlatController@getAllData');
Route::any('/getbyidkat/{id_kategori}', 'AlatController@getByIdKat');
Route::any('/getdataruang', 'RuangController@getDataRuang');
Route::any('/getdatainstalasi', 'InstalasiController@getDataInstalasi');
Route::any('/getdatakeluar', 'AlatKeluarController@getDataKeluar');
Route::any('/selectdatakeluar/{id}', 'AlatKeluarController@selectDataKeluar');
Route::any('/getdatamaintenance', 'MaintenanceController@getDataMaintenance');
Route::any('/selectdatamaintenance/{id}', 'MaintenanceController@selectDataMaintenance');
