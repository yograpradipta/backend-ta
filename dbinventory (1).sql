-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 22 Sep 2019 pada 09.09
-- Versi server: 10.1.37-MariaDB
-- Versi PHP: 7.3.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `dbinventory`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `admins`
--

CREATE TABLE `admins` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `nma_admin` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `username` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `notelp` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `alats`
--

CREATE TABLE `alats` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `id_bidang` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `id_instalasi` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `id_ruang` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `id_alat` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `serial_number` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `merek` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tanggal_masuk` date NOT NULL,
  `jadwal_kalibrasi` date NOT NULL,
  `id_distributor` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `alats`
--

INSERT INTO `alats` (`id`, `id_bidang`, `id_instalasi`, `id_ruang`, `id_alat`, `serial_number`, `merek`, `type`, `tanggal_masuk`, `jadwal_kalibrasi`, `id_distributor`, `created_at`, `updated_at`) VALUES
(7, '1', '2', '1', '5', '1', 'Litmann', 'Classic II', '2019-09-20', '2019-09-20', '1', '2019-09-19 18:33:46', '2019-09-19 18:33:46'),
(9, '1', '2', '2', '7', '11.2-LAU-01', 'MAK', '33605B', '2019-09-20', '2019-09-20', '1', '2019-09-19 18:39:09', '2019-09-19 18:39:09'),
(10, '3', '2', '3', '5', '12343', 'Triase', 'rt43', '2019-09-20', '2019-09-20', '1', '2019-09-19 20:22:19', '2019-09-19 20:22:19');

-- --------------------------------------------------------

--
-- Struktur dari tabel `alat_keluars`
--

CREATE TABLE `alat_keluars` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `id_alat` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sernum` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `merek` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tgl_masuk` date NOT NULL,
  `tgl_keluar` date NOT NULL,
  `id_ruang` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ket` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `alat_keluars`
--

INSERT INTO `alat_keluars` (`id`, `id_alat`, `sernum`, `merek`, `type`, `tgl_masuk`, `tgl_keluar`, `id_ruang`, `ket`, `created_at`, `updated_at`) VALUES
(12, 'Stetoskop/Stetoskop Dewasa', '00145.0.00000115', 'TESENA X-Ray Film Viewer', 'TSN 014-3', '2019-09-20', '2019-09-20', 'Ruangan Klinik Spesialis Penyakit Dalam', 'gugu', '2019-09-19 20:31:51', '2019-09-19 20:31:51');

-- --------------------------------------------------------

--
-- Struktur dari tabel `alat_masuks`
--

CREATE TABLE `alat_masuks` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `serial_number` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nama_alat` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `merek` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tanggal_masuk` date NOT NULL,
  `lokasi_ruangan` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `jadwal_kalibrasi` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `bidangs`
--

CREATE TABLE `bidangs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `nm_bidang` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ket` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `bidangs`
--

INSERT INTO `bidangs` (`id`, `nm_bidang`, `ket`, `created_at`, `updated_at`) VALUES
(1, 'bidang IGD', 'ini bidang IGD', '2019-09-17 02:47:27', NULL),
(2, 'Pelayanan Medik dan Keperawatan/Pelayanan Klinik RS', 'Pelayanan Medik dan Keperawatan/Pelayanan Klinik RS', '2019-09-04 09:30:19', '2019-09-19 20:11:38'),
(3, 'Penunjang Medik RS', 'Penunjang Medik RS', '2019-09-19 20:12:31', '2019-09-19 20:12:31');

-- --------------------------------------------------------

--
-- Struktur dari tabel `distributors`
--

CREATE TABLE `distributors` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `nm_distributor` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `no_telp` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `distributors`
--

INSERT INTO `distributors` (`id`, `nm_distributor`, `no_telp`, `created_at`, `updated_at`) VALUES
(1, 'PT. PARAMOUNT BED INDONESIA', '21332', '2019-09-04 07:04:26', '2019-09-19 20:24:57'),
(2, 'PT. MURTI INDAH SENTOSA', '09846', '2019-09-19 20:25:16', '2019-09-19 20:25:16');

-- --------------------------------------------------------

--
-- Struktur dari tabel `instalasis`
--

CREATE TABLE `instalasis` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `nm_instalasi` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `id_bidang` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ket` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `instalasis`
--

INSERT INTO `instalasis` (`id`, `nm_instalasi`, `id_bidang`, `ket`, `created_at`, `updated_at`) VALUES
(1, 'Instalasi Rawat Jalan/Poliklinik', '2', 'Instalasi Rawat Jalan/Poliklinik', '2019-09-04 06:37:12', '2019-09-19 20:13:13'),
(2, 'Instalasi Gawat Darurat', '2', 'Instalasi Gawat Darurat', '2019-09-04 09:30:37', '2019-09-19 20:13:46'),
(3, 'Unit Hemodialisa', '3', 'Unit Hemodialisa', '2019-09-19 20:15:11', '2019-09-19 20:15:11'),
(4, 'Instalasi Radiodiagnostik', '3', 'Instalasi Radiodiagnostik', '2019-09-19 20:15:42', '2019-09-19 20:15:42');

-- --------------------------------------------------------

--
-- Struktur dari tabel `kalibrasis`
--

CREATE TABLE `kalibrasis` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `sernum` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tgl_kalibrasi` date NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `kategori_alats`
--

CREATE TABLE `kategori_alats` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `nm_alat` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `jumlah` int(11) NOT NULL,
  `ket` text COLLATE utf8mb4_unicode_ci,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `kategori_alats`
--

INSERT INTO `kategori_alats` (`id`, `nm_alat`, `jumlah`, `ket`, `image`, `created_at`, `updated_at`) VALUES
(5, 'X-Ray Film Viewer', 2, 'X-Ray Film Viewer', 'DiezbSlYaBys7Iq7.png', '2019-09-19 18:31:26', '2019-09-19 20:22:20'),
(6, 'Stetoskop/Stetoskop Dewasa', 0, 'Stetoskop/Stetoskop Dewasa', 'bbyTygPYivJe9S6r.png', '2019-09-19 18:32:10', '2019-09-19 20:31:52'),
(7, 'Meja Periksa / Tempat tidur periksa / Examination Table', 1, 'Meja Periksa / Tempat tidur periksa / Examination Table', 'beKrGVR1labNR1MC.png', '2019-09-19 18:38:16', '2019-09-19 18:39:09');

-- --------------------------------------------------------

--
-- Struktur dari tabel `maintenances`
--

CREATE TABLE `maintenances` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `id_alat` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `id_kategori` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `id_ruang` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `id_user` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `serial_number` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `merek` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `waktu_request` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `waktu_response` date NOT NULL,
  `problem` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `analisa` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `hasil` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `icon` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `color_icon` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `maintenances`
--

INSERT INTO `maintenances` (`id`, `id_alat`, `id_kategori`, `id_ruang`, `id_user`, `serial_number`, `merek`, `type`, `waktu_request`, `waktu_response`, `problem`, `analisa`, `hasil`, `icon`, `color_icon`, `created_at`, `updated_at`) VALUES
(1, '7', '7', '2', '16', '11.2-LAU-01', 'MAK', '33605B', '2019-09-22 06:24:43', '2019-09-22', 'jijjihygygy', '-', '-', 'done_outline', 'primary', '2019-09-19 19:35:22', '2019-09-21 21:23:56'),
(2, '6', '6', '1', '16', '00145.0.00000115', 'TESENA X-Ray Film Viewer', 'TSN 014-3', '2019-09-22 06:39:45', '2019-09-22', 'ssghfj', 'alat kekurangan oli samping', 'Layak Pakai', 'done_outline', 'primary', '2019-09-19 20:29:29', '2019-09-21 23:39:45'),
(3, '5', '5', '1', '1', '1', 'Litmann', 'Classic II', '2019-09-22 06:27:20', '2019-09-20', 'fsdfsdfs', '-', '-', 'done_outline', 'primary', '2019-09-21 19:22:27', '2019-09-21 19:22:27');

-- --------------------------------------------------------

--
-- Struktur dari tabel `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(24, '2014_10_12_000000_create_users_table', 1),
(25, '2014_10_12_100000_create_password_resets_table', 1),
(26, '2019_07_22_043206_create_alats_table', 1),
(27, '2019_07_26_124016_create_alat_masuks_table', 1),
(28, '2019_07_27_053038_create_alat_keluars_table', 1),
(29, '2019_07_27_064055_create_admins_table', 1),
(30, '2019_07_27_074749_create_user_mobiles_table', 1),
(31, '2019_07_27_082628_create_kalibrasis_table', 1),
(32, '2019_07_31_144152_create_kategori_alats_table', 1),
(33, '2019_08_06_142124_create_bidangs_table', 1),
(34, '2019_08_06_150448_create_instalasis_table', 1),
(35, '2019_08_06_152905_create_ruangs_table', 1),
(36, '2019_08_23_125853_create_distributors_table', 1),
(37, '2016_06_01_000001_create_oauth_auth_codes_table', 2),
(38, '2016_06_01_000002_create_oauth_access_tokens_table', 2),
(39, '2016_06_01_000003_create_oauth_refresh_tokens_table', 2),
(40, '2016_06_01_000004_create_oauth_clients_table', 2),
(41, '2016_06_01_000005_create_oauth_personal_access_clients_table', 2),
(42, '2019_09_17_050346_create_maintenances_table', 3),
(43, '2019_09_17_123417_master', 4);

-- --------------------------------------------------------

--
-- Struktur dari tabel `oauth_access_tokens`
--

CREATE TABLE `oauth_access_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  `client_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `oauth_access_tokens`
--

INSERT INTO `oauth_access_tokens` (`id`, `user_id`, `client_id`, `name`, `scopes`, `revoked`, `created_at`, `updated_at`, `expires_at`) VALUES
('01b357376d982ba18b59025f83fcd6950766395c6a1a261b2854c9d0fc67e9fbaac6e21814e0571a', 25, 2, NULL, '[]', 0, '2019-09-16 21:12:59', '2019-09-16 21:12:59', '2020-09-17 04:12:59'),
('0789198add95594485bf6fe67ebb9b86ebc3782fa2bdfb4c61a5f076b3d461a3eb72ab76d2d578c9', 27, 2, NULL, '[]', 0, '2019-09-21 10:54:32', '2019-09-21 10:54:32', '2020-09-21 17:54:32'),
('0901538f5224825794757548b3fef9ad742fe8e7a45827cc9ee6ff5c5d0949d0ea5b321ca2945e11', 7, 2, NULL, '[]', 0, '2019-09-12 07:38:11', '2019-09-12 07:38:11', '2020-09-12 14:38:11'),
('0cd3fd315dc7b643fab70712d63c4f1db90b2d9c7b572614986bdbb99fc178fc182b14a73b05ff24', 3, 2, NULL, '[]', 0, '2019-09-11 09:22:45', '2019-09-11 09:22:45', '2020-09-11 16:22:45'),
('0f3ebcd8c27f6ac5ac77b782ea4310841ace0719e8ad122dc523393aa1eedfde5eeefe55df0aa2bf', 25, 2, NULL, '[]', 0, '2019-09-20 07:44:38', '2019-09-20 07:44:38', '2020-09-20 14:44:38'),
('11752156c7d226a7a09373ffa47c018e6980834dc2c00ce322647d1ddf3c44163ac2dadd1e2cd12b', 8, 1, 'TutsForWeb', '[]', 0, '2019-09-13 22:20:22', '2019-09-13 22:20:22', '2020-09-14 05:20:22'),
('1529bc4027da4782b4a6c4fb38df78166cec08615604c3c803f8d0dff5ece7db6c5292048849b835', 25, 2, NULL, '[]', 0, '2019-09-16 01:41:04', '2019-09-16 01:41:04', '2020-09-16 08:41:04'),
('156c20bd242daec6ac4c665f8cd7bf4f941fd0ae3fe60864f329639074c1194cbb24094b19208c0d', 25, 2, NULL, '[]', 0, '2019-09-16 01:33:02', '2019-09-16 01:33:02', '2020-09-16 08:33:02'),
('1624ed8dbf4fc981e4b411c57e84f57f214492feea8c9c469a24515284659e19ec2015afa46618d6', 24, 2, NULL, '[]', 0, '2019-09-15 21:44:29', '2019-09-15 21:44:29', '2020-09-16 04:44:29'),
('19ad1b0d0bb2d2d117585d5325af10af8bc4a149a34f6efe02fa0ac3f5fce6836348196aba38222d', 7, 2, NULL, '[]', 0, '2019-09-12 08:02:06', '2019-09-12 08:02:06', '2020-09-12 15:02:06'),
('20be1e4d715c50871c2629dcadcd1c0997c78314398123e72a05615e303fe7cb07c1490a4120527e', 25, 2, NULL, '[]', 0, '2019-09-16 01:00:15', '2019-09-16 01:00:15', '2020-09-16 08:00:15'),
('22c6ce6619cd9f84a2a2d48215fc83f29b81734cef7d5b1ac29b88794dcda29aae0d94154b6a4601', 25, 2, NULL, '[]', 0, '2019-09-16 01:42:14', '2019-09-16 01:42:14', '2020-09-16 08:42:14'),
('236efbb912f4913bc90a279f40e29defd04fa82607850aa4d81460ace838df5696cfcaeb082f9519', 16, 1, 'TutsForWeb', '[]', 0, '2019-09-15 07:20:58', '2019-09-15 07:20:58', '2020-09-15 14:20:58'),
('238c120853ede044b8d8e437d45eecb51f27d8dbebf1211326a9d213b9dc47e4d4525c68b3f66a14', 7, 2, NULL, '[]', 0, '2019-09-13 09:36:49', '2019-09-13 09:36:49', '2020-09-13 16:36:49'),
('27c35083a33cdc46ed754d290279051c469cc616cd54d7a98130e16d94d8cfda15bad13abf243026', 10, 1, 'TutsForWeb', '[]', 0, '2019-09-13 22:23:26', '2019-09-13 22:23:26', '2020-09-14 05:23:26'),
('2a44f02f34fbe7536d2a002200a7d6a4ec16e1b4d3b358eaee236f0c908448b31ea0180279a02426', 3, 1, 'TutsForWeb', '[]', 0, '2019-09-11 23:27:15', '2019-09-11 23:27:15', '2020-09-12 06:27:15'),
('2a95e05290a88aaf451547d8edaa7b76d2ab78ed5ce5f25fd535558ed8f8f9ca0f4b0effb6a35ad7', 27, 2, NULL, '[]', 0, '2019-09-20 21:36:18', '2019-09-20 21:36:18', '2020-09-21 04:36:18'),
('3b03a068d69ba953d89e0212b94ee7ecca5e542059c644e0488c52d3def961f412d0236d2c963960', 25, 2, NULL, '[]', 0, '2019-09-16 01:20:04', '2019-09-16 01:20:04', '2020-09-16 08:20:04'),
('3de48af27b7553f388c351b117c8c3490820457ea030d3129d3a39c7ad17c290abb4e3523e90bbcc', 7, 2, NULL, '[]', 0, '2019-09-12 08:27:40', '2019-09-12 08:27:40', '2020-09-12 15:27:40'),
('4157f4ff3e465bc8377eb02f5e61be63db360dcedba9e5fbb54ad1f7a46933a25bf28249cccbdc5d', 12, 1, 'TutsForWeb', '[]', 0, '2019-09-14 01:33:19', '2019-09-14 01:33:19', '2020-09-14 08:33:19'),
('45729de9f90e5aa1b266c3df1d8e7c08fccbc309bcfd398ffeaef89ca61db52fe4f30f3398de9060', 16, 2, NULL, '[]', 0, '2019-09-20 07:43:08', '2019-09-20 07:43:08', '2020-09-20 14:43:08'),
('45ffdbce70527d1db617067c8df52cee2e72ffdaf7c3c2b8dbd4ed40215581ca7af2c1079c3feedf', 16, 2, NULL, '[]', 0, '2019-09-21 19:21:34', '2019-09-21 19:21:34', '2020-09-22 02:21:34'),
('4bc27aa7db0d71434dc32cc3598461b775a91c45cbae45c0b26de3f4865ea2bcaab3e55afca11e89', 23, 1, 'TutsForWeb', '[]', 0, '2019-09-15 21:25:09', '2019-09-15 21:25:09', '2020-09-16 04:25:09'),
('578b380495786f675c72ca8c4401656f1eefdbfa36043b7d0989619eb64554ddd9da179e36b8f855', 25, 2, NULL, '[]', 0, '2019-09-16 01:29:45', '2019-09-16 01:29:45', '2020-09-16 08:29:45'),
('57c27b7d78f5689847621a285f3102145afc8ce6104697a8094f479aa7494ca954fe360ed54891c9', 7, 2, NULL, '[]', 0, '2019-09-11 23:49:24', '2019-09-11 23:49:24', '2020-09-12 06:49:24'),
('5856728046617092df462df77fc6e0f79626e24f8f41326e8c773b93fa676afd4208a7862264e2e4', 16, 2, NULL, '[]', 0, '2019-09-16 01:48:38', '2019-09-16 01:48:38', '2020-09-16 08:48:38'),
('6083eae63549e4ead8c45919e9c295bdbe7a07804d5de475ff382115cffccdfbde89dc5b708643af', 23, 2, NULL, '[]', 0, '2019-09-15 21:25:26', '2019-09-15 21:25:26', '2020-09-16 04:25:26'),
('61816cc5d8b6b070d12f983123f56d7ec62462937b88a5e2e5a92a92d45d0bd0cce0a9954501f4e5', 13, 1, 'TutsForWeb', '[]', 0, '2019-09-14 01:42:29', '2019-09-14 01:42:29', '2020-09-14 08:42:29'),
('6329dac431a9a209b93449555d0b983ee9c0eb43e8d7080dbd40181a925120c920939060ae8e410c', 25, 2, NULL, '[]', 0, '2019-09-20 08:05:53', '2019-09-20 08:05:53', '2020-09-20 15:05:53'),
('63bfcd1878d10d27f1314f0f74efecc11db6169d15971748e880d6892d3a552af0ebee2d6c8b93f0', 11, 1, 'TutsForWeb', '[]', 0, '2019-09-13 22:57:00', '2019-09-13 22:57:00', '2020-09-14 05:57:00'),
('64844a927a55c24ca842f7db78ed6e637dc368635955c4ec49b707c155a4cab879ef350d8646351e', 25, 2, NULL, '[]', 0, '2019-09-16 10:44:11', '2019-09-16 10:44:11', '2020-09-16 17:44:11'),
('665cba4d49b4a695f8c9f3c69ded4db10c34c59ffb9b6f8b707be9182745723fa0fdcb09c94bb3a6', 27, 2, NULL, '[]', 0, '2019-09-20 08:46:33', '2019-09-20 08:46:33', '2020-09-20 15:46:33'),
('698fb8b9a2b3ff39f7db66c6271a444012fe5d0c09424d579032a632332f05819b080745709b1f08', 25, 2, NULL, '[]', 0, '2019-09-16 00:54:51', '2019-09-16 00:54:51', '2020-09-16 07:54:51'),
('6ad62d255af0ac4ff0c112c333fc0986f80b83c6c316a992fa079e07748dcab4b3e4c1206ebe65ea', 7, 2, NULL, '[]', 0, '2019-09-11 23:48:17', '2019-09-11 23:48:17', '2020-09-12 06:48:17'),
('6b0d281c99ab1bcad5b6a3992047e08cc1c80ab673a9232045421760f33916dae3495d2376abbd9e', 7, 2, NULL, '[]', 0, '2019-09-12 06:09:12', '2019-09-12 06:09:12', '2020-09-12 13:09:12'),
('6e0c2926c96abf65dd726ea991608cfdc81e61b98994834dde9f75d76ac02d7caee2e5c5a29a20b0', 15, 1, 'TutsForWeb', '[]', 0, '2019-09-15 00:21:18', '2019-09-15 00:21:18', '2020-09-15 07:21:18'),
('6e8e921e1470377a5119743f767ee14e92ae13075cca6a9d7258186ed983862c1240d319f2f9222a', 25, 2, NULL, '[]', 0, '2019-09-16 01:44:34', '2019-09-16 01:44:34', '2020-09-16 08:44:34'),
('6fb9a02724381de9f81c152e8b1ebbf7d0f6a60ae49907ec39c311fe5e54b1f05a50d83ed08f6341', 7, 1, 'TutsForWeb', '[]', 0, '2019-09-11 23:47:32', '2019-09-11 23:47:32', '2020-09-12 06:47:32'),
('72d27c865b079da7683ff61d4865d2904db4941275091f5fd26639408084522d75ac50c65f4da1ea', 14, 2, NULL, '[]', 0, '2019-09-15 00:17:14', '2019-09-15 00:17:14', '2020-09-15 07:17:14'),
('739960a7b6fc2e1c04b9b28b0175daf49a78cd2425041e5d08be3d1766a28300b016e61300d70238', 27, 1, 'TutsForWeb', '[]', 0, '2019-09-20 07:45:14', '2019-09-20 07:45:14', '2020-09-20 14:45:14'),
('7408995ce8aa8104bd8c088e8bea45b44a1947307afa58e233dd3df390957783c1d4420011242a4f', 25, 2, NULL, '[]', 0, '2019-09-16 01:25:18', '2019-09-16 01:25:18', '2020-09-16 08:25:18'),
('7b968c816198de3cfe837390ed6fe3aece1df45f765eaf45c30de60c6e0754d9f78a8cc777935936', 19, 1, 'TutsForWeb', '[]', 0, '2019-09-15 08:19:41', '2019-09-15 08:19:41', '2020-09-15 15:19:41'),
('7bb0f9792a82e1dfac390c6b723c7c5206ade8ee5e1f3993d602de3e97199d43602c16167858a116', 3, 1, 'TutsForWeb', '[]', 0, '2019-09-11 09:15:04', '2019-09-11 09:15:04', '2020-09-11 16:15:04'),
('7d1e6b6e0afbba5766bf0cb6345691a85a2ca6449d254b212933bc058926b88f1c576b69b5987aea', 9, 1, 'TutsForWeb', '[]', 0, '2019-09-13 22:22:03', '2019-09-13 22:22:03', '2020-09-14 05:22:03'),
('7e73ad465a8c79467ab357b3b0fdcc8bdc9882d860b953df4201c4c7e8ce03f5870647ba50c1c1be', 7, 2, NULL, '[]', 0, '2019-09-12 06:13:18', '2019-09-12 06:13:18', '2020-09-12 13:13:18'),
('8122a6e4ce530eb374c2d8a808433b4976c257035592c191798eba3f43a2eec0fdc8680f466b4792', 18, 1, 'TutsForWeb', '[]', 0, '2019-09-15 07:59:55', '2019-09-15 07:59:55', '2020-09-15 14:59:55'),
('82acc526b622e10c9f32150af775eb10fe71a4b9c4374e5888d807702991cb5fa52174c8ec945b14', 5, 1, 'TutsForWeb', '[]', 0, '2019-09-11 23:41:58', '2019-09-11 23:41:58', '2020-09-12 06:41:58'),
('84cdcc5fa55a9edef81f0dfddb6c3e64ead9074ac94983c77f5fd79bd1c675e7cf040f0f21e62f0e', 7, 2, NULL, '[]', 0, '2019-09-13 22:08:52', '2019-09-13 22:08:52', '2020-09-14 05:08:52'),
('88f6eeff958716b3ba921fb3286b28ed9c516d71c5ca7c20ee1f859160010787452ba922b4cefed1', 25, 1, 'TutsForWeb', '[]', 0, '2019-09-16 00:23:42', '2019-09-16 00:23:42', '2020-09-16 07:23:42'),
('8ead06a8ec79a6e5bc08a50476d118375ff4c31ccb77be3e735044009e5e3a1fbe8e617c6a3510b3', 25, 2, NULL, '[]', 0, '2019-09-16 01:48:05', '2019-09-16 01:48:05', '2020-09-16 08:48:05'),
('8eb6cca3ad96a2f553a9e2c8f539fefb441b2cd2eb149e03d0ad5aeaf2264445b7a672fba4ce649b', 2, 1, 'TutsForWeb', '[]', 0, '2019-09-10 08:15:42', '2019-09-10 08:15:42', '2020-09-10 15:15:42'),
('928c4a535b879279cd5cd7cca4de9226de595a46bb4ee1791b641a19c6b8eacd0d438234d4b5204c', 3, 2, NULL, '[]', 0, '2019-09-11 23:29:07', '2019-09-11 23:29:07', '2020-09-12 06:29:07'),
('94df4ab94f8238a8fb5ea339a69e8a8e7de352c34164f733bcb53139c56a1f8ce7918de130632026', 25, 2, NULL, '[]', 0, '2019-09-20 07:45:16', '2019-09-20 07:45:16', '2020-09-20 14:45:16'),
('a0dab55ec2843b064e0366f9aaa82ca57acfdc4bb5d08e4df710a5e9448eaa0617f38f8380a28041', 2, 1, 'TutsForWeb', '[]', 0, '2019-09-10 08:16:38', '2019-09-10 08:16:38', '2020-09-10 15:16:38'),
('a2bf54f04f48825213acfa7c9ac62f14d5876d147afad6775756760e0f07f519e219e36ca8b9f8c7', 7, 2, NULL, '[]', 0, '2019-09-13 22:08:38', '2019-09-13 22:08:38', '2020-09-14 05:08:38'),
('a39b96b0f5185d94e7d780c62123f1b4bdbc40c1f4c8a62d490008c43fabd072b34aa736690d29b5', 22, 2, NULL, '[]', 0, '2019-09-15 21:21:33', '2019-09-15 21:21:33', '2020-09-16 04:21:33'),
('a60df4552592316713c164434739e79637aa0638f79929f866e557cbb4ee62a96568a6851e5281cf', 16, 2, NULL, '[]', 0, '2019-09-20 08:43:08', '2019-09-20 08:43:08', '2020-09-20 15:43:08'),
('ac13a2e411b894280b91f8e5425bab1b38cd6b2168dd94f75a5db7a296fc8f7b17b9d276ffe2abf7', 7, 2, NULL, '[]', 0, '2019-09-13 09:53:13', '2019-09-13 09:53:13', '2020-09-13 16:53:13'),
('ae38469b22c6635f3ccc486c6cb5caec9e7a2aca7dadf818799788610b2a333f705f17833d0efe50', 13, 1, 'TutsForWeb', '[]', 0, '2019-09-14 01:43:31', '2019-09-14 01:43:31', '2020-09-14 08:43:31'),
('af5322c7e09b9ff52f8690661fa6bcdf9ec7c9d54d30dd7fdaf7ecc1ea0a8a7bb23e53792a75b5ce', 24, 1, 'TutsForWeb', '[]', 0, '2019-09-15 21:44:22', '2019-09-15 21:44:22', '2020-09-16 04:44:22'),
('b48d33601b00a4b655d4c4fe4783e90098741f24ea71a9b432243ac24bad5bf69a03b65bee728dc8', 14, 1, 'TutsForWeb', '[]', 0, '2019-09-15 00:16:48', '2019-09-15 00:16:48', '2020-09-15 07:16:48'),
('b84564be5061296a3852585ce45a15547f4f5381b04c3a50852bdc0dad6296577e2d259cfc129b47', 4, 1, 'TutsForWeb', '[]', 0, '2019-09-11 23:39:31', '2019-09-11 23:39:31', '2020-09-12 06:39:31'),
('b84cda4128da503c91275852f52c1e4002d1a95264660b52eb2c17d305f5c216e8d357ab67949a04', 24, 2, NULL, '[]', 0, '2019-09-15 21:48:47', '2019-09-15 21:48:47', '2020-09-16 04:48:47'),
('baa5e13559e59bc451c28a2fb37c9160062ae0a9a3de47d7c0a5fdf8f30eccdc851887af5b6e18a4', 25, 2, NULL, '[]', 0, '2019-09-16 01:26:56', '2019-09-16 01:26:56', '2020-09-16 08:26:56'),
('c3b48901629647f386a11b494e4d8b2c982c2f9ee42d1e5c73cb4759b94975623e87f0714c33c02a', 7, 2, NULL, '[]', 0, '2019-09-12 06:35:34', '2019-09-12 06:35:34', '2020-09-12 13:35:34'),
('c5d47f7182b76fca3ba2e595e5d24c6fefa970a2eb13fe17d270a5774d2a768e6c43ab09dcaa19f8', 21, 1, 'TutsForWeb', '[]', 0, '2019-09-15 09:58:09', '2019-09-15 09:58:09', '2020-09-15 16:58:09'),
('caf8f3f7bc1831e8101c4e0036340628670d4608a73ca234ef264b403f78445233f5f253b083f240', 22, 1, 'TutsForWeb', '[]', 0, '2019-09-15 21:20:59', '2019-09-15 21:20:59', '2020-09-16 04:20:59'),
('cdba197f9821a93a9c93bf255ba433267de9ca25362b6326fa48b8a35481a2896f189531e9e3e8da', 7, 2, NULL, '[]', 0, '2019-09-11 23:48:50', '2019-09-11 23:48:50', '2020-09-12 06:48:50'),
('d035cec5c51b54d7a979744988d3295cfa94b32a72078da2318d24ba10f324c308b6222b8bde2343', 25, 2, NULL, '[]', 0, '2019-09-16 00:53:47', '2019-09-16 00:53:47', '2020-09-16 07:53:47'),
('d452505684be4d28448e82be504530c3528ec13489381b98cc4a7cf00c3947c8777e3e2f5e0fa1fa', 20, 1, 'TutsForWeb', '[]', 0, '2019-09-15 09:29:40', '2019-09-15 09:29:40', '2020-09-15 16:29:40'),
('d858ddec649f0eb4f1d34fb010f8698c0cd6fae0da9eb1aaa2dc7666235bc4ac5e723c634a4bdfc8', 25, 2, NULL, '[]', 0, '2019-09-16 01:39:48', '2019-09-16 01:39:48', '2020-09-16 08:39:48'),
('dd8f9a608b4fdec38207f5bbc5f22ab2c8efed98a8539208ccc455d6c2414d146d12de677223722b', 6, 1, 'TutsForWeb', '[]', 0, '2019-09-11 23:46:14', '2019-09-11 23:46:14', '2020-09-12 06:46:14'),
('ddcdd10606a52f22998131fb2e6305e6f517d50b8574131de07bd5d716e103480c431a0cb175d977', 16, 2, NULL, '[]', 0, '2019-09-16 01:51:45', '2019-09-16 01:51:45', '2020-09-16 08:51:45'),
('def5d62dabbadd6d74fc0b7c2d13073e7c96b8752cf0387b0bfa364c80cec43a1ab982225734a1ec', 7, 2, NULL, '[]', 0, '2019-09-12 08:02:49', '2019-09-12 08:02:49', '2020-09-12 15:02:49'),
('df3b6635360ccdb98de12d96b57ccc1d2b21951468c5527d35621b85eefcf0964178e31fdc7def04', 14, 2, NULL, '[]', 0, '2019-09-15 06:51:23', '2019-09-15 06:51:23', '2020-09-15 13:51:23'),
('e436b3e97a829ad64cff7a095561d908daf0fc8c60188098d13f4390cdba8edb28e3b46a4d7672cb', 10, 2, NULL, '[]', 0, '2019-09-14 23:47:29', '2019-09-14 23:47:29', '2020-09-15 06:47:29'),
('e7eab477488b8d7669e43f561fb6e6042c095063b7e2d4ecccd8a5807588d9e9ba59870351b396b8', 14, 1, 'TutsForWeb', '[]', 0, '2019-09-14 02:25:10', '2019-09-14 02:25:10', '2020-09-14 09:25:10'),
('ea8d4b822e35ed93f118d0162ae404ef94aa7ef232f67aa73187cbe94984ef1c2766d863580dce33', 2, 1, 'TutsForWeb', '[]', 0, '2019-09-10 08:16:45', '2019-09-10 08:16:45', '2020-09-10 15:16:45'),
('ebf80a04e74e37ed6edd50af3d1d800386e5664a624628fc7d12d1ccc5b606635e68fa93ba75d761', 16, 2, NULL, '[]', 0, '2019-09-16 01:01:38', '2019-09-16 01:01:38', '2020-09-16 08:01:38'),
('ed3f605e86b7298ac8022447b204db8db0d90b8b584df2aea7ac46043f03fcf597b5a08f5008154d', 16, 2, NULL, '[]', 0, '2019-09-16 00:26:24', '2019-09-16 00:26:24', '2020-09-16 07:26:24'),
('eeaf10c566d75af95cc856cc9c816c384198ef60c48d6f44fb7d3c5036b18f1b4b89b687ac1eaf10', 8, 1, 'TutsForWeb', '[]', 0, '2019-09-13 22:19:57', '2019-09-13 22:19:57', '2020-09-14 05:19:57'),
('f188c829303ea8e2b41a019669198b9dde73a274c61525c815df821413e2437557dbc280f8b6f8be', 7, 2, NULL, '[]', 0, '2019-09-12 06:36:04', '2019-09-12 06:36:04', '2020-09-12 13:36:04'),
('f6fd4782565af9f2c3b8b84b0edaf099b88736e2f774097c42ac2401ec609206c357e2265ff16ffe', 25, 2, NULL, '[]', 0, '2019-09-16 00:25:22', '2019-09-16 00:25:22', '2020-09-16 07:25:22'),
('f734c64b13d49cf02db94e745da1a69b48825dfc8537db004b4eb98ef20f74b9485e18492c9e4ca0', 26, 1, 'TutsForWeb', '[]', 0, '2019-09-16 00:24:43', '2019-09-16 00:24:43', '2020-09-16 07:24:43'),
('fb58ab6b157aa04cad510b9ad3143c75b1505d4075c606d8330e4fecacf31af46a967fae0b352a65', 3, 1, 'TutsForWeb', '[]', 0, '2019-09-11 09:15:17', '2019-09-11 09:15:17', '2020-09-11 16:15:17'),
('fb667ce9673f2f827cff7baceb0d6b5246ee84cee4541db14e2adda360c520d04c230b61ea789ea3', 7, 2, NULL, '[]', 0, '2019-09-12 08:29:00', '2019-09-12 08:29:00', '2020-09-12 15:29:00'),
('fba8618268b225a28d4621038b1182fe734703aaf7b87626e8742a60d2d8d5e038bb8adc41bc955e', 17, 1, 'TutsForWeb', '[]', 0, '2019-09-15 07:45:21', '2019-09-15 07:45:21', '2020-09-15 14:45:21');

-- --------------------------------------------------------

--
-- Struktur dari tabel `oauth_auth_codes`
--

CREATE TABLE `oauth_auth_codes` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `client_id` int(10) UNSIGNED NOT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `oauth_clients`
--

CREATE TABLE `oauth_clients` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `secret` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `redirect` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `personal_access_client` tinyint(1) NOT NULL,
  `password_client` tinyint(1) NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `oauth_clients`
--

INSERT INTO `oauth_clients` (`id`, `user_id`, `name`, `secret`, `redirect`, `personal_access_client`, `password_client`, `revoked`, `created_at`, `updated_at`) VALUES
(1, NULL, 'Laravel Personal Access Client', 'ZnM17AsEEWJMiU5MSTkLlCreW9mNWrpNgRAEgSUU', 'http://localhost', 1, 0, 0, '2019-09-10 07:45:47', '2019-09-10 07:45:47'),
(2, NULL, 'Laravel Password Grant Client', 'BKr9VuzcsL7z2BYv9IethrR60rUbqARDmciPCMZg', 'http://localhost', 0, 1, 0, '2019-09-10 07:45:48', '2019-09-10 07:45:48');

-- --------------------------------------------------------

--
-- Struktur dari tabel `oauth_personal_access_clients`
--

CREATE TABLE `oauth_personal_access_clients` (
  `id` int(10) UNSIGNED NOT NULL,
  `client_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `oauth_personal_access_clients`
--

INSERT INTO `oauth_personal_access_clients` (`id`, `client_id`, `created_at`, `updated_at`) VALUES
(1, 1, '2019-09-10 07:45:47', '2019-09-10 07:45:47');

-- --------------------------------------------------------

--
-- Struktur dari tabel `oauth_refresh_tokens`
--

CREATE TABLE `oauth_refresh_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `access_token_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `oauth_refresh_tokens`
--

INSERT INTO `oauth_refresh_tokens` (`id`, `access_token_id`, `revoked`, `expires_at`) VALUES
('07fcf7c299b7073423d8d820798539fc8a65dc755316594c9a5f425fe9b9252b54a73dd49d78554b', 'ebf80a04e74e37ed6edd50af3d1d800386e5664a624628fc7d12d1ccc5b606635e68fa93ba75d761', 0, '2020-09-16 08:01:38'),
('082b7d650f25758b5b243384242de5f084ba5170b9051d90b6913c339699f98c1563c77f49c8401f', '6e8e921e1470377a5119743f767ee14e92ae13075cca6a9d7258186ed983862c1240d319f2f9222a', 0, '2020-09-16 08:44:34'),
('0eca4718b475d377b46329dd0c52512825c8f905a560cb9e441cbeee1577c768056029b3b79b8ccf', 'fb667ce9673f2f827cff7baceb0d6b5246ee84cee4541db14e2adda360c520d04c230b61ea789ea3', 0, '2020-09-12 15:29:00'),
('0edade7918c07d653141325e3204cdf13911072b79b2f72d44339f2cb0d3e15cbcc70856ed51e33b', 'f188c829303ea8e2b41a019669198b9dde73a274c61525c815df821413e2437557dbc280f8b6f8be', 0, '2020-09-12 13:36:04'),
('12bc93efe05af770b3929bf3c9f41c1c201c75fdd932b65be773219d2aa15f57e2558d37680120ad', '45729de9f90e5aa1b266c3df1d8e7c08fccbc309bcfd398ffeaef89ca61db52fe4f30f3398de9060', 0, '2020-09-20 14:43:09'),
('1ea43f6e216e041b8da6841499da3123b73fc290d701d19045a0352510d2e997f4b84629e4a684ef', 'c3b48901629647f386a11b494e4d8b2c982c2f9ee42d1e5c73cb4759b94975623e87f0714c33c02a', 0, '2020-09-12 13:35:34'),
('2729d0b5dc3e5e162ad5379848f4a60a6f5897ff99cbda4e23e6b77c7dee84ce22b806a1564da053', 'ed3f605e86b7298ac8022447b204db8db0d90b8b584df2aea7ac46043f03fcf597b5a08f5008154d', 0, '2020-09-16 07:26:24'),
('2891fcf69acccf1c20db3eeb7c57ffd47c466f825a6104610351a37a589f579454b4366b98dddba7', '6ad62d255af0ac4ff0c112c333fc0986f80b83c6c316a992fa079e07748dcab4b3e4c1206ebe65ea', 0, '2020-09-12 06:48:17'),
('2ac9c1fc1bc21995a8b27245d6c67ae0b6a3ece56b5b2d7842610f436e48f3342c4ebffcce0ba73e', '6b0d281c99ab1bcad5b6a3992047e08cc1c80ab673a9232045421760f33916dae3495d2376abbd9e', 0, '2020-09-12 13:09:13'),
('2b3d01032b10a3066222ad48f175e3c8fe443aafaf318912d3897ee1ea29b3765e9f4b124170dafc', '0789198add95594485bf6fe67ebb9b86ebc3782fa2bdfb4c61a5f076b3d461a3eb72ab76d2d578c9', 0, '2020-09-21 17:54:32'),
('2dd345f14bc24ebb2e3af27f4ef8bfd1197102b6bd528ffee0183c3006cb38abdc755d6ace57b56b', '7408995ce8aa8104bd8c088e8bea45b44a1947307afa58e233dd3df390957783c1d4420011242a4f', 0, '2020-09-16 08:25:18'),
('37fdc6921c225136ca9ceab71e72321996d9a6d513af454fbe53d2e9c5eb10d9b7cec1e036c87956', '45ffdbce70527d1db617067c8df52cee2e72ffdaf7c3c2b8dbd4ed40215581ca7af2c1079c3feedf', 0, '2020-09-22 02:21:34'),
('3a066f79e2eb0365e939de614f3ee544545afbe8f5bd3d84a132f55a73e58392e7b2ced51171fcea', 'a60df4552592316713c164434739e79637aa0638f79929f866e557cbb4ee62a96568a6851e5281cf', 0, '2020-09-20 15:43:09'),
('3b5e58b121e6d5b658e8b0811840ecda05fa013fa6c2ea046891f2707fba56c2945cfe6bdb7cf0f8', '84cdcc5fa55a9edef81f0dfddb6c3e64ead9074ac94983c77f5fd79bd1c675e7cf040f0f21e62f0e', 0, '2020-09-14 05:08:52'),
('3cc7cc1e8588701256591638648cba151a8959c6c8470a5ad8990f4e73a39eb7ba02ba9c596611fa', '3de48af27b7553f388c351b117c8c3490820457ea030d3129d3a39c7ad17c290abb4e3523e90bbcc', 0, '2020-09-12 15:27:40'),
('3eedda2d2c8b6b493104c83483342fc78dffcd374638124ee3d18f6a7928c8b0fe113d92d5c07b4a', '0f3ebcd8c27f6ac5ac77b782ea4310841ace0719e8ad122dc523393aa1eedfde5eeefe55df0aa2bf', 0, '2020-09-20 14:44:38'),
('454fe1012122d0dbdf52d61c36842b34d104282ec1c69e17f7d4d62807d5b2c351e752d3e1959c4f', 'd858ddec649f0eb4f1d34fb010f8698c0cd6fae0da9eb1aaa2dc7666235bc4ac5e723c634a4bdfc8', 0, '2020-09-16 08:39:49'),
('472f30268191978186826866dc0e689a1e2b9a85f707342ff985e3f9d4dbb761352bc8094bd117f6', 'baa5e13559e59bc451c28a2fb37c9160062ae0a9a3de47d7c0a5fdf8f30eccdc851887af5b6e18a4', 0, '2020-09-16 08:26:57'),
('4a420fe310d85bfdb7e4e445df62e09a9a9ad94b7e1bbe9cc997a79ef8050be3a73858da208296a0', 'e436b3e97a829ad64cff7a095561d908daf0fc8c60188098d13f4390cdba8edb28e3b46a4d7672cb', 0, '2020-09-15 06:47:30'),
('4cdd8193f3105dab5b85873b6e76475ab83109d3637f3589a9b6267d5e1e2f11a74ba5f07b71f336', 'def5d62dabbadd6d74fc0b7c2d13073e7c96b8752cf0387b0bfa364c80cec43a1ab982225734a1ec', 0, '2020-09-12 15:02:49'),
('4f39e93e8f5a25d6e7efebe09bd26181931d347a48128166d6f517fc4d301b10ebb82bf79bd6bd49', '7e73ad465a8c79467ab357b3b0fdcc8bdc9882d860b953df4201c4c7e8ce03f5870647ba50c1c1be', 0, '2020-09-12 13:13:19'),
('537418769ab23fc7c685962edc94062d7cfabbba33188b57b3fe05f76668a01758699269752e9546', '72d27c865b079da7683ff61d4865d2904db4941275091f5fd26639408084522d75ac50c65f4da1ea', 0, '2020-09-15 07:17:14'),
('5bc43eb8b28e0ece690f98bda4c6e652925c901596eae71f8a71249a4b4bdfa6925ec57b805527b1', '94df4ab94f8238a8fb5ea339a69e8a8e7de352c34164f733bcb53139c56a1f8ce7918de130632026', 0, '2020-09-20 14:45:17'),
('60170e455d78f541565a352bfbbb5bf266570df77ca68ed754c0f1637a5c3b94e836e175165d9720', 'f6fd4782565af9f2c3b8b84b0edaf099b88736e2f774097c42ac2401ec609206c357e2265ff16ffe', 0, '2020-09-16 07:25:22'),
('76bd6aeb3b0e1880384ab995eeb168a9c5d51ad206ff70225a7b4cb3a6f7ed19ce35ea2382c70974', '01b357376d982ba18b59025f83fcd6950766395c6a1a261b2854c9d0fc67e9fbaac6e21814e0571a', 0, '2020-09-17 04:12:59'),
('7921ff115b59167435c4c4eb6bb4f3fc9a00bccb07f7558f4351a8d4377f392f37a5664430e6d998', '1529bc4027da4782b4a6c4fb38df78166cec08615604c3c803f8d0dff5ece7db6c5292048849b835', 0, '2020-09-16 08:41:04'),
('8e85bc2cb6e4e50b2e355cf44894c9e6c1c6a980e5500be0d8eaa48debc5867636167f6fe19391f9', '0901538f5224825794757548b3fef9ad742fe8e7a45827cc9ee6ff5c5d0949d0ea5b321ca2945e11', 0, '2020-09-12 14:38:11'),
('93c46dfd730f67fd32ba9353c3bf18d5f5143d2c76bfb758014977aa22bb540cf667aa26a6b5b1b7', '0cd3fd315dc7b643fab70712d63c4f1db90b2d9c7b572614986bdbb99fc178fc182b14a73b05ff24', 0, '2020-09-11 16:22:46'),
('9a6e854a6d80150b1547561af53db9961cb59e850390a4a69720d9b6ad276d43d67dc43f01ce6146', '57c27b7d78f5689847621a285f3102145afc8ce6104697a8094f479aa7494ca954fe360ed54891c9', 0, '2020-09-12 06:49:24'),
('9d1e3abba9783a3ce141e9e77f3a4505bf895c2a27ab544b662ad5db6caad18f2fb1f6cec456170e', '2a95e05290a88aaf451547d8edaa7b76d2ab78ed5ce5f25fd535558ed8f8f9ca0f4b0effb6a35ad7', 0, '2020-09-21 04:36:18'),
('a1309eafcfd4bcd8f1f4e0644cc30e0ae706cb7e0442d2c7a78d26c77ac5281c9c9bdb3721dc5f34', 'cdba197f9821a93a9c93bf255ba433267de9ca25362b6326fa48b8a35481a2896f189531e9e3e8da', 0, '2020-09-12 06:48:50'),
('a551586f7b687d95551c050fadb344123785851424bf99ae4b42a1072586a6f986d73c03f53192b2', '20be1e4d715c50871c2629dcadcd1c0997c78314398123e72a05615e303fe7cb07c1490a4120527e', 0, '2020-09-16 08:00:15'),
('a6a661d89fdbd0bac7ad0ee1130b7fe39fad25f94c0de4171bd1acdad365da876bc66e24532f8e7d', '6329dac431a9a209b93449555d0b983ee9c0eb43e8d7080dbd40181a925120c920939060ae8e410c', 0, '2020-09-20 15:05:53'),
('ae65687192f9c0771be4c01749885f0f9a253265d3aae275414317291eecb579dd88a17c47dd9c3a', 'a39b96b0f5185d94e7d780c62123f1b4bdbc40c1f4c8a62d490008c43fabd072b34aa736690d29b5', 0, '2020-09-16 04:21:33'),
('ae8c2c3bd2c7eab5ab86abe0506566e3c95feaf008d9146cbb164cd5c5c87fb71a780bbe926cd8ad', '6083eae63549e4ead8c45919e9c295bdbe7a07804d5de475ff382115cffccdfbde89dc5b708643af', 0, '2020-09-16 04:25:27'),
('b14379ec18ebd9aa2ed11acfc71adc711629413b8db27b2df62f57f342bda6de8b42e9561b71f48b', '22c6ce6619cd9f84a2a2d48215fc83f29b81734cef7d5b1ac29b88794dcda29aae0d94154b6a4601', 0, '2020-09-16 08:42:15'),
('b46f170bda3fb1e9b23219dc841d9bf367ecc52d9d2f1748a53c57f5b613ec4d1e2bebedc02e9f25', 'd035cec5c51b54d7a979744988d3295cfa94b32a72078da2318d24ba10f324c308b6222b8bde2343', 0, '2020-09-16 07:53:48'),
('b51ca307681bf58b56333db2ce266b25e2ce21e282b3bb9ded9f4a8cdadd6d4c06ed1d9ebafa60ed', '19ad1b0d0bb2d2d117585d5325af10af8bc4a149a34f6efe02fa0ac3f5fce6836348196aba38222d', 0, '2020-09-12 15:02:07'),
('bc18624b61595b6d94dd1dcdf74b7d586a01402509ac240059093e02b2414c264248db78c30af462', 'b84cda4128da503c91275852f52c1e4002d1a95264660b52eb2c17d305f5c216e8d357ab67949a04', 0, '2020-09-16 04:48:47'),
('c2bc41f692b908bfe1afd2f21ebfc48537f97099d6547e3d232eab0c9123e150ae999778bf80e2cd', '64844a927a55c24ca842f7db78ed6e637dc368635955c4ec49b707c155a4cab879ef350d8646351e', 0, '2020-09-16 17:44:12'),
('c52cd70652901dce2296f6d5bff2795c3ebc88e1fa5917d39d3f953893318e41b383cdc981a0e044', '1624ed8dbf4fc981e4b411c57e84f57f214492feea8c9c469a24515284659e19ec2015afa46618d6', 0, '2020-09-16 04:44:30'),
('c9ae56334895cf00ac5c0328e116f118ba9f30771599ca91cb670f6ff655923bc83e25a4c2874da6', '5856728046617092df462df77fc6e0f79626e24f8f41326e8c773b93fa676afd4208a7862264e2e4', 0, '2020-09-16 08:48:38'),
('d07ae3a542089214804d4f311e764e057b7cdc5f7936ff7505b6828824fc393ea4ed866ddd59d0c4', '3b03a068d69ba953d89e0212b94ee7ecca5e542059c644e0488c52d3def961f412d0236d2c963960', 0, '2020-09-16 08:20:05'),
('d47f61d4adc8632a5e8c1c2347b743bf569eafee317201d11612248b6de11cfd7f1319d2265ac9de', '665cba4d49b4a695f8c9f3c69ded4db10c34c59ffb9b6f8b707be9182745723fa0fdcb09c94bb3a6', 0, '2020-09-20 15:46:33'),
('d6c7b5a5718c2ee09613e4f37e855500bfb19b4eeb7ada0bdf59cc6c9eb49d50c54ceec81d977311', '156c20bd242daec6ac4c665f8cd7bf4f941fd0ae3fe60864f329639074c1194cbb24094b19208c0d', 0, '2020-09-16 08:33:02'),
('e07d632bdcffb7a397d74fd0621c59c7ce17d7b47a983a3d788125ed1dc298a0b87d06bc808f9551', '578b380495786f675c72ca8c4401656f1eefdbfa36043b7d0989619eb64554ddd9da179e36b8f855', 0, '2020-09-16 08:29:46'),
('e6fbd75d033b8b368af20d9b2b9a703159f300b60d70d6d9c7da4ee30d4c2d360aa680b051bb8951', 'a2bf54f04f48825213acfa7c9ac62f14d5876d147afad6775756760e0f07f519e219e36ca8b9f8c7', 0, '2020-09-14 05:08:38'),
('e75a2945b75a6574739e44da353173d4f8f2d9d461d564d77e225cdbd421692ef8e4df8be2db67e5', '238c120853ede044b8d8e437d45eecb51f27d8dbebf1211326a9d213b9dc47e4d4525c68b3f66a14', 0, '2020-09-13 16:36:50'),
('ea3b97964bc166545aba408869671a66b67f601f08be254b95890f04c932999d071129b7c14edca8', 'df3b6635360ccdb98de12d96b57ccc1d2b21951468c5527d35621b85eefcf0964178e31fdc7def04', 0, '2020-09-15 13:51:24'),
('f21b760f1f1cb29edaafb437b4de2629013579c9855f0aab5c11d66868f6ac5eb76077542469b656', '8ead06a8ec79a6e5bc08a50476d118375ff4c31ccb77be3e735044009e5e3a1fbe8e617c6a3510b3', 0, '2020-09-16 08:48:06'),
('f2211807f83ec367d563917a7c820d690d3d7cbeca421cf7a2da04aed59f887e99ada8c5ca5ddf58', '928c4a535b879279cd5cd7cca4de9226de595a46bb4ee1791b641a19c6b8eacd0d438234d4b5204c', 0, '2020-09-12 06:29:07'),
('fc39233890757602a5bd0e92383eeabe0c55dd800bcbddcf5922434aea1eea6834829e55baccd22c', '698fb8b9a2b3ff39f7db66c6271a444012fe5d0c09424d579032a632332f05819b080745709b1f08', 0, '2020-09-16 07:54:51'),
('fd7891c31174e0594e2f1fc1bfdff014f70b832964f39401f25c9681ac6a4e4045865550c6c0b337', 'ac13a2e411b894280b91f8e5425bab1b38cd6b2168dd94f75a5db7a296fc8f7b17b9d276ffe2abf7', 0, '2020-09-13 16:53:13'),
('fdbf7a540f914cb44935dff1e6ce51b6a3c8a1aec4551de6d542b29fbeb365720ba6c815f4c1c8b6', 'ddcdd10606a52f22998131fb2e6305e6f517d50b8574131de07bd5d716e103480c431a0cb175d977', 0, '2020-09-16 08:51:46');

-- --------------------------------------------------------

--
-- Struktur dari tabel `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `ruangs`
--

CREATE TABLE `ruangs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `nm_ruang` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `id_bidang` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `id_instalasi` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ket` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `ruangs`
--

INSERT INTO `ruangs` (`id`, `nm_ruang`, `id_bidang`, `id_instalasi`, `ket`, `created_at`, `updated_at`) VALUES
(1, 'Ruangan Klinik Spesialis Penyakit Dalam', '2', '1', 'Ruangan Klinik Spesialis Penyakit Dalam', '2019-09-04 06:29:54', '2019-09-19 20:16:53'),
(2, 'Ruangan Klinik Spesialis Kesehatan Anak', '2', '1', 'Ruangan Klinik Spesialis Kesehatan Anak', '2019-09-04 06:43:27', '2019-09-19 20:17:57'),
(3, 'Ruangan Triase', '2', '2', 'Ruangan Triase', '2019-09-19 20:19:44', '2019-09-19 20:19:44');

-- --------------------------------------------------------

--
-- Struktur dari tabel `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `notelp` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `role` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `notelp`, `role`, `remember_token`, `created_at`, `updated_at`) VALUES
(16, 'user', 'user@gmail.com', NULL, '$2y$10$aF.7C7lgkwsH7VmWY0gmNOs57VIAz8BPUGUeCgYbx0AnU1K1VqS.a', '0823456', 'user', 'HquGUTSvah', '2019-09-15 07:20:56', '2019-09-15 07:20:56'),
(22, 'admin07', 'admin07@gmail.com', NULL, '$2y$10$4KiefEMZqvvnNWgK/chAt.G0Zdaj6hH8k2js4mjXUHW3vPfy/96nm', '098753', 'admin', 'RFSuathKbE', '2019-09-15 21:20:56', '2019-09-15 21:22:00'),
(23, 'yogra', 'yogra07@gmail.com', NULL, '$2y$10$cZAEo2GBfl1bSOlAfoouLeYIKYK9S.9aPZmpsDNFhBWtKcIQucq2a', '12345667', 'admin', 'VAAVhGciRy', '2019-09-15 21:25:08', '2019-09-15 21:36:03'),
(24, 'yogra', 'yogra02@gmail.com', NULL, '$2y$10$wHVwm2PUmaX7n.PX5P/P/O497FvJ6ejU2XY9wS7QhzmI.hwUxyMSm', '12233', 'admin', 'rvstehR7nq', '2019-09-15 21:44:22', '2019-09-15 21:48:14'),
(25, 'admin', 'admin@gmail.com', NULL, '$2y$10$tScz/GGviYZ24YC4niK2U.7jGCWN1BZU6rYhk76GapPaZAe0Mbr9O', '1234354566', 'admin', 'Pi5rcFGNzM', '2019-09-16 00:23:35', '2019-09-16 00:23:35'),
(26, 'user', 'user2@gmail.com', NULL, '$2y$10$orDfHbY0uVs3ZnEHedoHpOh3ihJBnVkanAZd50Kjot5FAguUEb/Qi', '12345', 'user', 'dGvG8ANo0q', '2019-09-16 00:24:42', '2019-09-16 00:24:42'),
(27, 'yogra', 'yogra@gmail.com', NULL, '$2y$10$vVA60SvGP3w.sxsXbvZOs.8j5JDGczd9IFFkoaTadkBD4NvoJjLO.', '12233', 'admin', 'dppciDUl4k', '2019-09-20 07:45:12', '2019-09-20 07:45:12');

-- --------------------------------------------------------

--
-- Struktur dari tabel `user_mobiles`
--

CREATE TABLE `user_mobiles` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `nma_user` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `username` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `notelp` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `admins`
--
ALTER TABLE `admins`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `admins_username_unique` (`username`);

--
-- Indeks untuk tabel `alats`
--
ALTER TABLE `alats`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `alats_serial_number_unique` (`serial_number`);

--
-- Indeks untuk tabel `alat_keluars`
--
ALTER TABLE `alat_keluars`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `alat_keluars_sernum_keluar_unique` (`sernum`);

--
-- Indeks untuk tabel `alat_masuks`
--
ALTER TABLE `alat_masuks`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `alat_masuks_serial_number_unique` (`serial_number`);

--
-- Indeks untuk tabel `bidangs`
--
ALTER TABLE `bidangs`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `distributors`
--
ALTER TABLE `distributors`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `instalasis`
--
ALTER TABLE `instalasis`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `kalibrasis`
--
ALTER TABLE `kalibrasis`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `kalibrasis_sernum_unique` (`sernum`);

--
-- Indeks untuk tabel `kategori_alats`
--
ALTER TABLE `kategori_alats`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `maintenances`
--
ALTER TABLE `maintenances`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `oauth_access_tokens`
--
ALTER TABLE `oauth_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_access_tokens_user_id_index` (`user_id`);

--
-- Indeks untuk tabel `oauth_auth_codes`
--
ALTER TABLE `oauth_auth_codes`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `oauth_clients`
--
ALTER TABLE `oauth_clients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_clients_user_id_index` (`user_id`);

--
-- Indeks untuk tabel `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_personal_access_clients_client_id_index` (`client_id`);

--
-- Indeks untuk tabel `oauth_refresh_tokens`
--
ALTER TABLE `oauth_refresh_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_refresh_tokens_access_token_id_index` (`access_token_id`);

--
-- Indeks untuk tabel `ruangs`
--
ALTER TABLE `ruangs`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `user_mobiles`
--
ALTER TABLE `user_mobiles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `user_mobiles_username_unique` (`username`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `admins`
--
ALTER TABLE `admins`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `alats`
--
ALTER TABLE `alats`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT untuk tabel `alat_keluars`
--
ALTER TABLE `alat_keluars`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT untuk tabel `alat_masuks`
--
ALTER TABLE `alat_masuks`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `bidangs`
--
ALTER TABLE `bidangs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT untuk tabel `distributors`
--
ALTER TABLE `distributors`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `instalasis`
--
ALTER TABLE `instalasis`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT untuk tabel `kalibrasis`
--
ALTER TABLE `kalibrasis`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `kategori_alats`
--
ALTER TABLE `kategori_alats`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT untuk tabel `maintenances`
--
ALTER TABLE `maintenances`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT untuk tabel `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=44;

--
-- AUTO_INCREMENT untuk tabel `oauth_clients`
--
ALTER TABLE `oauth_clients`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `ruangs`
--
ALTER TABLE `ruangs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT untuk tabel `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT untuk tabel `user_mobiles`
--
ALTER TABLE `user_mobiles`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
