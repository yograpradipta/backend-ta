<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SecretKeys extends Model
{
    //
    protected $table = 'oauth_clients';
    protected $fillable = [
        'secret'
    ];
}
