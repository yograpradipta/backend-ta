<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class bidang extends Model
{
    protected $table='bidangs';
    protected $fillable=[
      'nm_bidang','ket'
    ];
}
