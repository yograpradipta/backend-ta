<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Alat extends Model
{
    protected $table='alats';
    protected $fillable=[
      'id_ruang','id_alat','serial_number','merek','type','tanggal_masuk','jadwal_kalibrasi','id_distributor'
    ];
}
