<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class instalasi extends Model
{
    protected $table='instalasis';
    protected $fillable=[
      'nm_instalasi','id_bidang','ket'
    ];
}
