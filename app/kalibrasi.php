<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class kalibrasi extends Model
{
    protected $table='kalibrasis';
    protected $fillable=[
      'sernum','tgl_kalibrasi'
    ];
}
