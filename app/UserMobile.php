<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserMobile extends Model
{
  protected $table='user_mobiles';
  protected $fillable=[
    'nma_user','username','password','notelp'
  ];
}
