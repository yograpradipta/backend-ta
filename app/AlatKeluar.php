<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AlatKeluar extends Model
{
    protected $table='alat_keluars';
    protected $fillable=[
      'id_alat','sernum','merek','type','tgl_masuk','tgl_keluar','id_ruang','ket'
    ];
}
