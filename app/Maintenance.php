<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Maintenance extends Model
{
    protected $table='maintenances';
    protected $fillable=[
      'id_alat','id_kategori','id_ruang','id_user','serial_number','merek','type','waktu_request','waktu_response','problem','analisa','hasil','icon','color_icon'
    ];
}
