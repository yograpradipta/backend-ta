<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AlatMasuk extends Model
{
    protected $table='alat_masuks';
    protected $fillable=[
      'serial_number','nama_alat','merek','type','tanggal_masuk','lokasi_ruangan','jadwal_kalibrasi','image'
    ];
}
