<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class KategoriAlat extends Model
{
    protected $table="kategori_alats";
    protected $fillable=[
        'nm_alat','jumlah','ket','image'
    ];
}
