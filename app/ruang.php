<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ruang extends Model
{
    protected $table='ruangs';
    protected $fillable=[
      'nm_ruang','id_instalasi','ket'
    ];
}
