<?php

namespace App\Http\Controllers;

use App\Admin;
use Illuminate\Http\Request;

class AdminController extends Controller
{

    public function index()
    {
      return Admin::all();
    }

    public function store(Request $request)
    {
        $this->validate($request,[
          'nma_admin'=>'required',
          'username'=>'required',
          'password'=>'required',
          'notelp'=>'required'
        ]);
        $data=Admin::create($request->all());
        return $data;
    }

    public function show($id)
    {
        $dataAdmin[]=Admin::find($id);
        if (count($dataAdmin)>0) {
          return response()->json($dataAdmin);
        }
        return response()->json(['error'=>'data tidak tersedia'],404);
    }

    public function update(Request $request, $id)
    {
        $data=Admin::find($id);
        $data->update($request->all());
        return response()->json($data);
    }

    public function destroy($id)
    {
        try {
          Admin::destroy($id);
          return response()->json(['delete success!'],204);
        } catch (\Exception $e) {
          return response()->json(['error'=>'delete cancel!'. $e],500);
        }

    }
}
