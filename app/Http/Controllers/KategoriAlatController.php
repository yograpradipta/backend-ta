<?php

namespace App\Http\Controllers;

use App\KategoriAlat;
use App\Alat;
use Illuminate\Http\Request;

class KategoriAlatController extends Controller
{
    public function index()
    {
        return KategoriAlat::all();
    }

    public function store(Request $request)
    {
        $data= $request->except('_token');
        try{
            if($request->has('image')) {
                $exploded = explode(',', $request->image);
                $decoded = base64_decode($exploded[1]);
                if (str_contains($exploded[0], 'jpg')) {
                  $extension= 'jpg';
                }
                else {
                  $extension= 'png';
                }
                $fileName = str_random() . '.' . $extension;
                $path = public_path() . '/imageAlat/' . $fileName;
                file_put_contents($path, $decoded);
                return KategoriAlat::create($request->except('image') + ['image' => $fileName]);
            }
            return KategoriAlat::create($data);

        }
        catch(\Exception $e){
             return response()-> json(['error'=> $e . 'input gagal!'],500);
        }

    }


    public function show($id)
    {
        $kategoriAlat[]= KategoriAlat::find($id);
        if (count($kategoriAlat)>0) {
            return response()->json($kategoriAlat);
        }
        return response()->json(['error'=>'data tidak tersedia'],404);
    }

    public function update(Request $request, $id)
    {
        $kategoriAlat=KategoriAlat::find($id);
        $kategoriAlat->update($request->all());
        return response()->json($kategoriAlat);
    }


    public function destroy($id)
    {
        try{
            KategoriAlat::destroy($id);
            return response(['delete success'], 204);
        }
        catch(\Exception $e){
            return response(['deleted error ' . $e], 500);
        }
    }

    // public function getDataKategori(Request $request){
    //     return KategoriAlat::join('alats','alats.id_alat','=','kategori_alats.id')
    //     ->join('ruangs','alats.id_ruang','=','ruangs.id')
    //     ->join('instalasis','ruangs.id_instalasi','=','instalasis.id')
    //     ->select('alats.merek','alats.type','kategori_alats.nm_alat','ruangs.nm_ruang','instalasis.nm_instalasi')
    //     ->orderBy('alats.id','desc')->get();
    // }


}
