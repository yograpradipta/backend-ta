<?php

namespace App\Http\Controllers;

use App\distributor;
use Illuminate\Http\Request;

class DistributorController extends Controller
{

    public function index()
    {
        return distributor::All();
    }

    public function store(Request $request)
    {
        $this->validate($request,[
            'nm_distributor'=>'required',
            'no_telp'=>'required',
            ]);
        $data=distributor::create($request->all());
        return $data;
    }

    public function show($id)
    {
        $data[]=distributor::find($id);
        if($data>0){
            return response()->json($data);
        }
        return response()->json(['error'=>'data tidak tersedia'],204);
    }

    public function update(Request $request, $id)
    {
        $data=distributor::find($id);
        $data->update($request->all());
        return response()->json($data);
    }


    public function destroy($id)
    {
        try{
            distributor::destroy($id);
            return response()->json(["delete succes"], 204);
        }catch(\Exception $e){
            return response()->json(["deleted problem:" . $e], 500);
        }
    }
}
