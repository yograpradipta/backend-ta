<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use App\bidang;
use App\instalasi;


class ComboController extends Controller
{
    public function bidang(): JsonResponse
    {
        return response()->json(bidang::orderBy('nm_bidang', 'ASC')->get());
    }

    public function instalasi(): JsonResponse
    {
        $instalasi = instalasi::whereIdBidang(request('bidang'))
            ->orderBy('nm_instalasi', 'ASC')
            ->get();
        return response()->json($instalasi);
    }
}

