<?php

namespace App\Http\Controllers;

use App\bidang;
use Illuminate\Http\Request;

class BidangController extends Controller
{

    public function index()
    {
        return bidang::all();
    }

    public function store(Request $request)
    {
        $this->validate($request,[
            'nm_bidang'=>'required',
            'ket'=>'required'
        ]);
        $data=bidang::create($request->all());
        return $data;
    }

    public function show($id)
    {
        $databidang[]= bidang::find($id);
        if ($databidang>0) {
            return response()->json($databidang);
        }
        return response()->json(["error"=>"data tidak tersedia!"], 404);
    }


    public function update(Request $request, $id)
    {
        $databidang=bidang::find($id);
        $databidang->update($request->all());
        return response()->json($databidang);
    }

    public function destroy($id)
    {
        try{
            bidang::destroy($id);
            return response(["deleted succes"],204);
        }
        catch(\Exception $e){
            return response(["deleted problem: " . $e], 500);
        }
    }
}
