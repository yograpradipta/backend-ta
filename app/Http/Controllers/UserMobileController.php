<?php

namespace App\Http\Controllers;

use App\UserMobile;
use Illuminate\Http\Request;

class UserMobileController extends Controller
{

    public function index()
    {
        return UserMobile::all();
    }

    public function store(Request $request)
    {
          $this->validate($request,[
            'nma_user'=>'required',
            'username'=>'required',
            'password'=>'required',
            'notelp'=>'required'
          ]);
          $data=UserMobile::create($request->all());
          return $data;
    }

    public function show($id)
    {
      $dataUser[]=UserMobile::find($id);
      if (count($dataUser)>0) {
        return response()->json($dataUser);
      }
      return response()->json(['error'=>'data tidak tersedia'],404);

    }

    public function update(Request $request, $id)
    {
      $data=UserMobile::find($id);
      $data->update($request->all());
      return response()->json($data);

    }

    public function destroy($id)
    {
        try {
          UserMobile::destroy($id);
          return response()->json(['delete success'],204);
        } catch (\Exception $e) {
          return response()->json(['error'=>'delete cancel'. $e],500);
        }

    }
}
