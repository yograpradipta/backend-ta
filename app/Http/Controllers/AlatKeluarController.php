<?php

namespace App\Http\Controllers;

use App\AlatKeluar;
use App\KategoriAlat;
use Illuminate\Http\Request;
use Response;
class AlatKeluarController extends Controller
{

    public function index()
    {
        return AlatKeluar::all();
    }

    public function store(Request $request)
    {
        $this->validate($request,[
          'id_alat'=>'required',
          'sernum'=>'required',
          'merek'=>'required',
          'type'=>'required',
          'tgl_masuk'=>'required',
          'tgl_keluar'=>'required',
          'id_ruang'=>'required',
          'ket'=>'required'
        ]);
        $data=AlatKeluar::create($request->all());
        return $data;
    }

    public function show($id)
    {
        $datakeluar[]=AlatKeluar::find($id);
        if (count($datakeluar)>0){
          return response()->json($datakeluar);
        }
        return response()->json(['error'=>'data tidak tersedia'],404);
    }

    public function update(Request $request, $id)
    {
        $data=AlatKeluar::find($id);
        $data->update($request->all());
        return response()->json($data);
    }

    public function destroy($id)
    {
      try{
         AlatKeluar::destroy($id);
         return response(['delete success!'],204);
      }catch(\Exception $e){
         return response(['Dalete Problem: ' . $e], 500);
      }
    }

    public function keluar($id)
   {
    //    $this->validate($request,[
    //        'id' => 'required' ]);
       $ktg = KategoriAlat ::find($id);
       $stokBaru = $ktg->jumlah - 1;
       $ktg->update(['jumlah' => $stokBaru]);
       return $ktg;
   }

   public function getDataKeluar(Request $request){
         return AlatKeluar::join('kategori_alats','alat_keluars.id_alat','=','kategori_alats.id')
        ->join('ruangs','alat_keluars.id_ruang','=','ruangs.id')
        ->join('instalasis','ruangs.id_instalasi','=','instalasis.id')
        ->join('bidangs','instalasis.id_bidang','=','bidangs.id')
        ->select('alat_keluars.sernum','alat_keluars.merek','kategori_alats.nm_alat',
        'ruangs.nm_ruang','instalasis.nm_instalasi','bidangs.nm_bidang','kategori_alats.image',
        'alat_keluars.type','alat_keluars.ket','alat_keluars.id','alat_keluars.tgl_masuk','alat_keluars.tgl_keluar')
        // ->where('alats.id_alat','=', $idkat)
        ->orderBy('alat_keluars.id','desc')->get();
    }
    public function selectDataKeluar($idkat){
         return AlatKeluar::join('kategori_alats','alat_keluars.id_alat','=','kategori_alats.id')
        ->join('ruangs','alat_keluars.id_ruang','=','ruangs.id')
        ->join('instalasis','ruangs.id_instalasi','=','instalasis.id')
        ->join('bidangs','instalasis.id_bidang','=','bidangs.id')
        ->select('alat_keluars.sernum','alat_keluars.merek','kategori_alats.nm_alat',
        'ruangs.nm_ruang','instalasis.nm_instalasi','bidangs.nm_bidang','kategori_alats.image',
        'alat_keluars.type','alat_keluars.ket','alat_keluars.id','alat_keluars.tgl_masuk','alat_keluars.tgl_keluar')
        ->where('alat_keluars.id','=', $idkat)
        ->orderBy('alat_keluars.id','desc')->get();
    }


}
