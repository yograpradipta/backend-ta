<?php

namespace App\Http\Controllers\API;

use App\rc;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Support\Str;
use Validator;
use Illuminate\Support\Facades\Auth;


class UserController extends Controller
{
    protected $model = 'User';

    protected $relation = [];


    public $successStatus=200;

    public function index()
    {
      return User::all();
    }

    public function show($id)
    {
        $user[]= User::find($id);
        if (count($user)>0)
          return response()->json($user);
        return response()-> json(['error' => 'alat tidak tersedia'],404);
    }

    public function role($role){
        $tampil = User::where('role','=',$role)->get();
        return response()->json($tampil);
    }

    public function login(Request $request){
        $credentials = [
          'email' => $request->email,
            'password' => $request->password,
            'role'=>$request->role
        ];
        if(\auth()->attempt($credentials)){
            $token = \auth()->user()->createToken('TutsForWeb')->accessToken;
//            $user = Auth::user();
//            $success['token'] = $user->createToken('MyApp')->accessToken;
            return response()->json(['token'=>$token],200);
        }else{
            return response()->json(['error'=>'Unautorised'],401);
        }
    }

    public function register(Request $request){
        $this->validate($request,[
           'name' => 'required|min:3',
            'email' => 'required|email|unique:users',
            'password' => 'required',
            'notelp' => 'required',
            'role' => 'required'
        ]);
        $user = User::create([
           'name' => $request->name,
            'email' => $request->email,
            'password' => bcrypt($request->password),
            'notelp' => $request->notelp,
            'role' => $request->role,
            'remember_token' => Str::random(10)

        ]);
        $token = $user->createToken('TutsForWeb')->accessToken;
        return response()->json(['token' => $token],200);
    }

    public function details(){
        $user = Auth::user();
        return response()->json(['success'=>$user],$this->successStatus);
    }

    public function getRole($email){
        $tampil = user::where('email','=',$email)->get();
        return response()->json($tampil);
    }

    public function destroy($id)
    {
      try{
         User::destroy($id);
         return response(['delete success!'],204);
      }catch(\Exception $e){
         return response(['Dalete Problem: ' . $e], 500);
      }
    }

    public function update(Request $request, $id)
    {
        $data=User::find($id);
        $data->name=$request->name;
        $data->email=$request->email;
        $data->notelp=$request->notelp;
        $data->role=$request->role;
        $data->password=bcrypt($request->password);
        $data->save();
        return response()->json($data);
        // $tampil = User::where('role','=',$id)->get();
        // $tampil->update($request->all());
        // return response()->json($tampil);
    }

}
