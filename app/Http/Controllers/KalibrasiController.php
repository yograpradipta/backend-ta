<?php

namespace App\Http\Controllers;

use App\kalibrasi;
use Illuminate\Http\Request;

class KalibrasiController extends Controller
{

    public function index()
    {
        return kalibrasi::all();
    }
    public function store(Request $request)
    {
        $this->validate($request,[
          'sernum'=>'required',
          'tgl_kalibrasi'=>'required'
        ]);
        $data=kalibrasi::create($request->all());
        return $data;
    }
    public function show($id)
    {
        $dataKalibrasi[]=kalibrasi::find($id);
        if (count($dataKalibrasi)>0) {
          return response()->json($dataKalibrasi);
        }
        return response()->json(['error'=>'data tidak tersedia'],404);
    }
    public function update(Request $request, $id)
    {
        $dataKalibrasi=kalibrasi::find($id);
        $dataKalibrasi->update($request->all());
        return response()->json($dataKalibrasi);
    }

    public function destroy($id)
    {
        try {
          kalibrasi::destroy($id);
          return response()->json(['delete success'],204);
        } catch (\Exception $e) {
          return response()->json(['error'=>'delete cancel'. $e],500);
        }

    }
}
