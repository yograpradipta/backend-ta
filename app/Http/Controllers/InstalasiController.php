<?php

namespace App\Http\Controllers;

use App\instalasi;
use Illuminate\Http\Request;

class InstalasiController extends Controller
{

    public function index()
    {
        return instalasi::all();
    }

    public function store(Request $request)
    {
        $this->validate($request,[
            'nm_instalasi'=>'required',
            'id_bidang'=>'required',
            'ket'=>'required'
        ]);
        $data=instalasi::create($request->all());
        return $data;
    }

    public function show($id)
    {
        $data[]=instalasi::find($id);
        if ($data>0) {
            return response()->json($data);
        }
        return response()->json(["error"=>"data tidak tersedia"], 204);
    }

    public function update(Request $request, $id)
    {
        $data=instalasi::find($id);
        $data->update($request->all());
        return response()->json($data);
    }

    public function destroy($id)
    {
        try{
            instalasi::destroy($id);
            return response()->json(["deleted succes"], 204);
        }
        catch(\Exception $e){
            return response()->json(["delete problem: " . $e], 500);
        }
    }

    public function getDataInstalasi(Request $request){
        return Instalasi::join('bidangs','instalasis.id_bidang','=','bidangs.id')
        ->select('instalasis.nm_instalasi','bidangs.nm_bidang','instalasis.id','instalasis.ket')
        ->orderBy('instalasis.id','asc')->get();
    }
}
