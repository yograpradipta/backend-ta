<?php

namespace App\Http\Controllers;

use App\Maintenance;
use Illuminate\Http\Request;

class MaintenanceController extends Controller
{
    protected $model = 'Alat';
    protected $relation = [];

    public function index()
    {
        return Maintenance::all();
    }

    public function store(Request $request)
    {
        $this->validate($request,[
          'id_alat'=>'required',
          'id_kategori'=>'required',
          'id_ruang'=>'required',
          'id_user'=>'required',
          'serial_number'=>'required',
          'merek'=>'required',
          'type'=>'required',
        //   'waktu_request'=>'required',
          'waktu_response'=>'required',
          'problem'=>'required',
          'analisa'=>'required',
          'hasil'=>'required',
          'icon'=>'required',
          'color_icon'=>'required'
        ]);
        $data=Maintenance::create($request->all());
        return $data;
    }

    public function show($id)
    {
        $datamaintenance[]= Maintenance::find($id);
        if (count($datamaintenance)>0)
          return response()->json($datamaintenance);
        return response()-> json(['error' => 'alat tidak tersedia'],404);
    }

    public function update(Request $request, $id)
    {
        $data=Maintenance::find($id);
        $data->update($request->all());
        return response()->json($data);
    }

    public function destroy($id)
    {
        try{
            Maintenance::destroy($id);
            return response(['delete success!'],204);
        }catch(\Exception $e){
            return response(['Dalete Problem: ' . $e], 500);
        }
    }

      public function getDataMaintenance(Request $request){
         return Maintenance::join('kategori_alats','maintenances.id_kategori','=','kategori_alats.id')
        ->join('ruangs','maintenances.id_ruang','=','ruangs.id')
        ->join('instalasis','ruangs.id_instalasi','=','instalasis.id')
        ->join('bidangs','instalasis.id_bidang','=','bidangs.id')
        ->join('alats','maintenances.id_alat','=','alats.id')
        ->join('users','maintenances.id_user','=','users.id')
        ->select('maintenances.merek','kategori_alats.nm_alat','users.name',
        'ruangs.nm_ruang','instalasis.nm_instalasi','bidangs.nm_bidang','kategori_alats.image',
        'maintenances.type','maintenances.id','maintenances.waktu_request','maintenances.serial_number',
        'maintenances.waktu_response','maintenances.problem','maintenances.analisa','maintenances.hasil',
        'maintenances.icon','maintenances.color_icon')
        // ->where('alat_keluars.id','=', $idkat)
        ->orderBy('maintenances.id','desc')->get();
    }
          public function selectDataMaintenance($idkat){
         return Maintenance::join('kategori_alats','maintenances.id_kategori','=','kategori_alats.id')
        ->join('ruangs','maintenances.id_ruang','=','ruangs.id')
        ->join('instalasis','ruangs.id_instalasi','=','instalasis.id')
        ->join('bidangs','instalasis.id_bidang','=','bidangs.id')
        ->join('alats','maintenances.id_alat','=','alats.id')
        ->join('users','maintenances.id_user','=','users.id')
        ->select('maintenances.merek','kategori_alats.nm_alat','users.name',
        'ruangs.nm_ruang','instalasis.nm_instalasi','bidangs.nm_bidang','kategori_alats.image',
        'maintenances.type','maintenances.id','maintenances.waktu_request','maintenances.serial_number',
        'maintenances.waktu_response','maintenances.problem','maintenances.analisa','maintenances.hasil',
        'maintenances.icon','maintenances.color_icon','users.notelp')
        ->where('maintenances.id','=', $idkat)
        ->orderBy('maintenances.id','desc')->get();
    }
}
