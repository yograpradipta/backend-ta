<?php

namespace App\Http\Controllers;

use App\AlatMasuk;
use Illuminate\Http\Request;

class AlatMasukController extends Controller
{

    public function index()
    {
        return AlatMasuk::all();
    }

    public function store(Request $request)
    {
        $this->validate($request,[
            'serial_number'=>'required',
            'nama_alat'=>'required',
            'merek'=>'required',
            'type'=>'required',
            'tanggal_masuk'=>'required',
            'lokasi_ruangan'=>'required',
            'jadwal_kalibrasi'=>'required',
            'image'=>'required'
        ]);
        $data=AlatMasuk::create($request->all());
        return $data;
    }

    public function show($id)
    {
        $datamasuk[]= AlatMasuk::find($id);
        if (count($datamasuk)>0)
          return response()->json($datamasuk);
        return response()-> json(['error' => 'data tidak tersedia'],404);
    }


    public function update(Request $request, $id)
    {
        $data=AlatMasuk::find($id);
        $data->update($request->all());
        return response()->json($data);
    }

    public function destroy($id)
    {
        try {
          AlatMasuk::destroy($id);
          return response(['delete success!'],204);
        } catch (\Exception $e) {
          return response(['delete Problem'.$e], 500);
        }

    }
}
