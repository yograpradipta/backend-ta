<?php

namespace App\Http\Controllers;

use App\ruang;
use Illuminate\Http\Request;

class RuangController extends Controller
{

    public function index()
    {
        return Ruang::all();
    }

    public function store(Request $request)
    {
        $this->validate($request,[
            'nm_ruang'=>'required',
            'id_instalasi'=>'required',
            'ket'=>'required'
        ]);
        $data=ruang::create($request->all());
        return $data;
    }

    public function show($id)
    {
        $data[]=ruang::find($id);
        if ($data>0) {
            return response()->json($data);
        }
        return response()->json(["error"=>"data tidak tersedia"], 204);
    }

    public function update(Request $request, $id)
    {
        $data=ruang::find($id);
        $data->update($request->all());
        return response()->json($data);
    }

    public function destroy($id)
    {
        try{
            ruang::destroy($id);
            return response()->json(["deleted succes"], 204);
        }
        catch(\Exception $e){
            return response()->json(["delete problem: " . $e], 500);
        }
    }
    public function getDataRuang(Request $request){
        return Ruang::join('instalasis','ruangs.id_instalasi','=','instalasis.id')
        ->join('bidangs','instalasis.id_bidang','=','bidangs.id')
        ->select('ruangs.nm_ruang','instalasis.nm_instalasi','bidangs.nm_bidang','ruangs.id','ruangs.ket')
        ->orderBy('ruangs.id','desc')->get();
    }
}
