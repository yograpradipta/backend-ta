<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Alat;
use App\KategoriAlat;

class AlatController extends Controller
{
    protected $model = 'Alat';
    protected $relation = [];

    public function index()
    {
        return Alat::all();
    }

    public function store(Request $request)
    {
        $this->validate($request,[
        //   'id_bidang'=>'required',
        //   'id_instalasi'=>'required',
          'id_ruang'=>'required',
          'id_alat'=>'required',
          'serial_number'=>'required',
          'merek'=>'required',
          'type'=>'required',
          'tanggal_masuk'=>'required',
          'jadwal_kalibrasi'=>'required',
          'id_distributor'=>'required'
        ]);
        $data=Alat::create($request->all());
        return $data;


    /*    $data=$request->except('_token');
        try{
          if($request->has('image')){
            $exploded = explode(',', $request->image);
            $decoded = base64_decode($exploded[1]);
            if (str_contains($exploded[0], 'jpg')) {
              $extension= 'jpg';
            }
            else {
              $extension= 'png';
            }
            $fileName = str_random() . '.' . $extension;
            $path = public_path() . '/imageAlat/' . $fileName;
            file_put_contents($path, $decoded);
            return Alat::create($request->except('image') + ['image' => $fileName]);
          }
          return Alat::create($data);
        }
        catch(\Exception $e){
          return response()-> json(['error' => $e .'input gagal'],500);
        }*/
    }

    public function show($id)
    {
        $dataalat[]= Alat::find($id);
        if (count($dataalat)>0)
          return response()->json($dataalat);
        return response()-> json(['error' => 'alat tidak tersedia'],404);
    }

    public function showbykat($idkat)
    {
        if($idkat==0){
            $tampil=Alat::all();
        }
        else{
            $tampil = Alat::where('id_alat','=',$idkat)->get();
        }
        return response()->json($tampil);
    }

    public function update(Request $request, $id)
    {
        $data=Alat::find($id);
        $data->update($request->all());
        return response()->json($data);
    }

    public function tambah($idkat){
        $ktg = KategoriAlat ::find($idkat);
        $stokBaru = $ktg->jumlah + 1;
        $ktg->update(['jumlah' => $stokBaru]);
        return $ktg;
    }

    // public function edit(Request $request, $id )
    // {
    //     $this->validate($request, [
        // 'id' => 'required',
        // 'id_bidang'=>'required',
        //   'id_instalasi'=>'required',
        //   'id_ruang'=>'required',
        //   'id_alat'=>'required',
        //   'serial_number'=>'required',
        //   'merek'=>'required',
        //   'type'=>'required',
        //   'tanggal_masuk'=>'required',
        //   'jadwal_kalibrasi'=>'required',
        //   'id_distributor'=>'required'
        // 'jumMasuk' => 'required'
        // ]);
        // $idkat = Alat::find($request->id_alat);




        // $tampil = Alat::where('id_alat','=',$id)->get();
        // $tampil->update($request->all());







        // DB::update('update alats set merek = ? where id_alat = ?', array('Update postingan baru', 2));
        // $edit= $tampil->update([
		// 	'id_bidang' => $request->id_bidang,
		// 	'id_instalasi' => $request->id_instalasi,
		// 	'id_ruang' => $request->id_ruang]);
        // $ttl = $request->jumMasuk + $jumlah->jumlah;
        // $jumlah->update(['jumlah' => $ttl]);
        // return $jumlah;
    //     return response()->json($tampil);

    // }

    public function destroy($id)
    {
      try{
         Alat::destroy($id);
         return response(['delete success!'],204);
      }catch(\Exception $e){
         return response(['Dalete Problem: ' . $e], 500);
      }
    }

    public function getAllData(Request $request){
        return Alat::join('kategori_alats','alats.id_alat','=','kategori_alats.id')
        ->join('ruangs','alats.id_ruang','=','ruangs.id')
        ->join('instalasis','ruangs.id_instalasi','=','instalasis.id')
        ->join('bidangs','instalasis.id_bidang','=','bidangs.id')
        ->join('distributors','alats.id_distributor','=','distributors.id')
        ->select('alats.serial_number','alats.merek','alats.type','kategori_alats.nm_alat','alats.tanggal_masuk','jadwal_kalibrasi','ruangs.nm_ruang','instalasis.nm_instalasi'
        ,'bidangs.nm_bidang','distributors.nm_distributor','kategori_alats.image','alats.id')
        ->orderBy('alats.id','desc')->get();
    }

    public function getByIdKat($idkat){
        return Alat::join('kategori_alats','alats.id_alat','=','kategori_alats.id')
        ->join('ruangs','alats.id_ruang','=','ruangs.id')
        ->join('instalasis','ruangs.id_instalasi','=','instalasis.id')
        ->join('bidangs','instalasis.id_bidang','=','bidangs.id')
        ->join('distributors','alats.id_distributor','=','distributors.id')
        ->select('alats.serial_number','alats.merek','alats.type','kategori_alats.nm_alat','alats.tanggal_masuk','jadwal_kalibrasi','ruangs.nm_ruang','instalasis.nm_instalasi'
        ,'bidangs.nm_bidang','distributors.nm_distributor','kategori_alats.image','alats.id','alats.id_alat')
        ->where('alats.id_alat','=', $idkat)
        ->orderBy('alats.id','desc')->get();
    }
}
