<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class distributor extends Model
{
    protected $table='distributors';
    protected $fillable=[
        'nm_distributor','no_telp'
    ];
}
