<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAlatsTable extends Migration
{

    public function up()
    {
        Schema::create('alats', function (Blueprint $table) {
            $table->bigIncrements('id');
            // $table->string('id_bidang');
            // $table->string('id_instalasi');
            $table->string('id_ruang');
            $table->string('id_alat');
            // $table->string('serial_number');
            $table->string('serial_number',20)->unique();
            $table->string('merek');
            $table->string('type');
            $table->date('tanggal_masuk');
            $table->date('jadwal_kalibrasi');
            $table->string('id_distributor');
            $table->timestamps();
        });
    }


    public function down()
    {
        Schema::dropIfExists('alats');
    }
}
