<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAlatKeluarsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('alat_keluars', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('id_alat');
            $table->string('sernum',20)->unique();
            $table->string('merek');
            $table->string('type');
            $table->string('tgl_masuk');
            $table->date('tgl_keluar');
            $table->string('id_ruang');
            $table->string('ket');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('alat_keluars');
    }
}
