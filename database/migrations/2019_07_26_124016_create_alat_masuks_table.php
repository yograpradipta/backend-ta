<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAlatMasuksTable extends Migration
{
    public function up()
    {
        Schema::create('alat_masuks', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('serial_number',20)->unique();
            $table->string('nama_alat');
            $table->string('merek');
            $table->string('type');
            $table->date('tanggal_masuk');
            $table->string('lokasi_ruangan');
            $table->string('jadwal_kalibrasi');
            $table->string('image')->nullable;
            $table->timestamps();
        });
    }
    public function down()
    {
        Schema::dropIfExists('alat_masuks');
    }
}
