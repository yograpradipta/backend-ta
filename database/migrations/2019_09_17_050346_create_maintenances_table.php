<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMaintenancesTable extends Migration
{

    public function up()
    {
        Schema::create('maintenances', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('id_alat');
            $table->string('id_kategori');
            $table->string('id_ruang');
            $table->string('id_user');
            $table->string('serial_number',20);
            $table->string('merek');
            $table->string('type');
            $table->timestamp('waktu_request');
            $table->date('waktu_response');
            $table->string('problem');
            $table->string('analisa');
            $table->string('hasil');
            $table->string('icon');
            $table->string('color_icon');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('maintenances');
    }
}
