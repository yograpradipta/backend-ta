<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateKalibrasisTable extends Migration
{

    public function up()
    {
        Schema::create('kalibrasis', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('sernum',20)->unique();
            $table->date('tgl_kalibrasi');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('kalibrasis');
    }
}
