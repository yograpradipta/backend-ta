<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateKategoriAlatsTable extends Migration
{

    public function up()
    {
        Schema::create('kategori_alats', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nm_alat');
            $table->integer('jumlah');
            $table->text('ket')->nullable();
            $table->string('image')->nullable();
            $table->timestamps();
        });
    }
    public function down()
    {
        Schema::dropIfExists('kategori_alats');
    }
}
